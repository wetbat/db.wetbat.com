-- MySQL dump 10.13  Distrib 8.0.32, for Win64 (x86_64)
--
-- Host: host.docker.internal    Database: wetbat
-- ------------------------------------------------------
-- Server version	5.7.41

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `addresses` (
  `id` varchar(36) NOT NULL,
  `street` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresses`
--

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` VALUES ('262da751-005d-4dbb-8525-5088d723df86','95700 Roissy-en-France, France','Paris','Roissy-en-France','France'),('5137d50a-f40e-408d-80b4-0d25a838e7d6','Av. dos Libaneses, 3503 - Tirirical, São Luís - MA, 65056-480','São Luís','Maranhão','Brazil'),('82787d16-c519-42ff-ba98-bac45a541eeb','Rod. Hélio Smidt, s/nº - Aeroporto, Guarulhos - SP, 07190-100','Guarulhos','São Paulo','Brazil'),('fe7bedfe-6a37-4d74-9744-d7509307d86d','Queens, NY 11430, United States','New York','New York','United States');
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `airports`
--

DROP TABLE IF EXISTS `airports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `airports` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `addressId` varchar(36) DEFAULT NULL,
  `locationId` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `REL_1587b5ddcee15189288c3b7b9d` (`addressId`),
  UNIQUE KEY `REL_e588eb6835ebdd99fcc71d1133` (`locationId`),
  CONSTRAINT `FK_1587b5ddcee15189288c3b7b9d6` FOREIGN KEY (`addressId`) REFERENCES `addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_e588eb6835ebdd99fcc71d11330` FOREIGN KEY (`locationId`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `airports`
--

LOCK TABLES `airports` WRITE;
/*!40000 ALTER TABLE `airports` DISABLE KEYS */;
INSERT INTO `airports` VALUES ('0027ed65-1091-49ac-830b-8179f0af4271','Aeroporto Internacional de Guarulhos (GRU)','82787d16-c519-42ff-ba98-bac45a541eeb','9688c1fb-fcf5-4c72-8b9f-0f56ecc64b43'),('55ef578f-fc46-4a68-be56-6e1b10b4d010','Aéroport de Paris-Charles de Gaulle (CDG)','262da751-005d-4dbb-8525-5088d723df86','fe42d389-b520-43d8-9d6b-6b339263a0bb'),('8bb922e4-dfcd-4214-8f2a-57abb93e76a9','John F. Kennedy International Airport (JFK)','fe7bedfe-6a37-4d74-9744-d7509307d86d','172081f4-2f90-4649-a46a-193dfcc57523'),('f718be50-5276-4310-998c-c3ed78e19872','Aeroporto Internacional de São Luís (SLZ)','5137d50a-f40e-408d-80b4-0d25a838e7d6','f779b0d6-7d68-4ca7-bdc6-98a3a9fc0b9b');
/*!40000 ALTER TABLE `airports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `arrivals`
--

DROP TABLE IF EXISTS `arrivals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `arrivals` (
  `id` varchar(36) NOT NULL,
  `date` datetime NOT NULL,
  `airportId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_e3f2edbc7d8ce677d3c2678cb93` (`airportId`),
  CONSTRAINT `FK_e3f2edbc7d8ce677d3c2678cb93` FOREIGN KEY (`airportId`) REFERENCES `airports` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `arrivals`
--

LOCK TABLES `arrivals` WRITE;
/*!40000 ALTER TABLE `arrivals` DISABLE KEYS */;
/*!40000 ALTER TABLE `arrivals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contacts` (
  `id` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departures`
--

DROP TABLE IF EXISTS `departures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `departures` (
  `id` varchar(36) NOT NULL,
  `date` datetime NOT NULL,
  `airportId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_94890e152b28d66edec8de9c62e` (`airportId`),
  CONSTRAINT `FK_94890e152b28d66edec8de9c62e` FOREIGN KEY (`airportId`) REFERENCES `airports` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departures`
--

LOCK TABLES `departures` WRITE;
/*!40000 ALTER TABLE `departures` DISABLE KEYS */;
/*!40000 ALTER TABLE `departures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `invoices` (
  `id` varchar(255) NOT NULL,
  `serviceId` varchar(255) NOT NULL,
  `status` enum('PENDING','PAID','CANCELED') NOT NULL,
  `statusChangedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `REL_6475a17527a8cec5ba7c98821b` (`serviceId`),
  CONSTRAINT `FK_6475a17527a8cec5ba7c98821bc` FOREIGN KEY (`serviceId`) REFERENCES `services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices`
--

LOCK TABLES `invoices` WRITE;
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `locations` (
  `id` varchar(36) NOT NULL,
  `latitude` decimal(11,9) NOT NULL,
  `longitude` decimal(12,9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES ('172081f4-2f90-4649-a46a-193dfcc57523',40.644261400,-73.775629000),('9688c1fb-fcf5-4c72-8b9f-0f56ecc64b43',-23.426234300,-46.480007700),('f779b0d6-7d68-4ca7-bdc6-98a3a9fc0b9b',-2.583553000,-44.236409400),('fe42d389-b520-43d8-9d6b-6b339263a0bb',49.007859700,2.550771500);
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quotes`
--

DROP TABLE IF EXISTS `quotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `quotes` (
  `id` varchar(255) NOT NULL,
  `transportation` varchar(255) NOT NULL,
  `contactId` varchar(36) DEFAULT NULL,
  `travalers` int(11) NOT NULL,
  `arrivalId` varchar(36) DEFAULT NULL,
  `departureId` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `REL_4bf53b33a11c8a004cc6075433` (`contactId`),
  KEY `FK_6bc793860da720a67f093c39860` (`arrivalId`),
  KEY `FK_8dd3ed0ab125a7bf2703ec60c58` (`departureId`),
  CONSTRAINT `FK_4bf53b33a11c8a004cc60754335` FOREIGN KEY (`contactId`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_6bc793860da720a67f093c39860` FOREIGN KEY (`arrivalId`) REFERENCES `arrivals` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_8dd3ed0ab125a7bf2703ec60c58` FOREIGN KEY (`departureId`) REFERENCES `departures` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quotes`
--

LOCK TABLES `quotes` WRITE;
/*!40000 ALTER TABLE `quotes` DISABLE KEYS */;
/*!40000 ALTER TABLE `quotes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `services` (
  `transportation` varchar(255) NOT NULL,
  `travalers` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `orderedAt` datetime NOT NULL,
  `id` varchar(255) NOT NULL,
  `departureId` varchar(36) DEFAULT NULL,
  `arrivalId` varchar(36) DEFAULT NULL,
  `contactId` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `REL_e13df19d200ce5f774d2a08943` (`contactId`),
  KEY `FK_2014dd872f987d14f1c31c1f03f` (`departureId`),
  KEY `FK_731dc7640e7b2442cb30f8d9360` (`arrivalId`),
  CONSTRAINT `FK_2014dd872f987d14f1c31c1f03f` FOREIGN KEY (`departureId`) REFERENCES `departures` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_731dc7640e7b2442cb30f8d9360` FOREIGN KEY (`arrivalId`) REFERENCES `arrivals` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_e13df19d200ce5f774d2a089430` FOREIGN KEY (`contactId`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-10-18  4:24:17
